﻿using Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CovidCoreMap.Services
{
    public class Transactions
    {
        public List<CountrysModel> GetRTesults()
        {
            List<CountrysModel> countrys = new List<CountrysModel>();
            CountrysModel Country = new CountrysModel();
            Country.Country_Name = "Mexico";
            Country.deceased = 1489454;
            Country.infected = 2446577;
            Country.cured = 98954;
            Country.States = GenerateStates();
            countrys.Add(Country);


            return countrys;

        }
        public List<EstatesModel> GenerateStates()
        {
            List<EstatesModel> States = new List<EstatesModel>();
            EstatesModel State = new EstatesModel();

            State.State_Name = "Mexico";
            State.infected = 76354;
            State.cured = 2323;
            State.deceased = 3242342;
            State.CitysHall = GenerateCityHall();

            States.Add(State);

            State.State_Name = "Hidalgo";
            State.infected = 232423;
            State.cured = 5454;
            State.deceased = 54646;
            State.CitysHall = GenerateCityHall();

            States.Add(State);

            State.State_Name = "Durango";
            State.infected = 78567;
            State.cured = 56745;
            State.deceased = 34772;
            State.CitysHall = GenerateCityHall();

            States.Add(State);


            State.State_Name = "Oaxaca";
            State.infected = 657;
            State.cured = 657765;
            State.deceased = 5675675;
            State.CitysHall = GenerateCityHall();


            return States;
        }


        public List<CityHallModel> GenerateCityHall()
        {
            List<CityHallModel> AllHalls = new List<CityHallModel>();
            CityHallModel Hall = new CityHallModel();

            Hall.Hall_Name = "Tlalnepantla";
            Hall.infected = 76354;
            Hall.cured = 2323;
            Hall.deceased = 3242342;

            AllHalls.Add(Hall);


            return AllHalls;
        }
    }
}
